import javax.swing.*;
import java.awt.event.* ;
import java.awt.BorderLayout ; 

public class RemoteControl extends JFrame{

    private static final long serialVersionUID = 1L;
    JButton displayButton, playButton, closeButton ; 
    private final JPanel buttonPanel;
    private static JTextArea text_area;
    JScrollPane scrollPanel;
    JMenuBar menu;
    JToolBar tools ; 

    public RemoteControl() {

        setLayout(new BorderLayout());
        ////////////////////////////
        ///////// Boutons //////////
        ////////////////////////////
        buttonPanel = new JPanel();

        final Display display = new Display("Display");
        displayButton = new JButton(display);
        buttonPanel.add(displayButton);

        final Play play = new Play("Play");
        playButton = new JButton(play);
        buttonPanel.add(playButton);

        final Close close = new Close("Close");
        closeButton = new JButton(close);
        buttonPanel.add(closeButton);

        add(buttonPanel, BorderLayout.SOUTH);

        //////////////////////////////////////////////
        ///////// Zone de texte (defilable) //////////
        //////////////////////////////////////////////
        text_area = new JTextArea(5, 50);
        scrollPanel = new JScrollPane(text_area);
        add(scrollPanel, BorderLayout.CENTER);

        text_area.append("Bienvenue dans la telecommande C++\nVeuillez effectuer une action\n");

        //////////////////////////////////
        ///////// Barre de Menu //////////
        //////////////////////////////////
        menu = new JMenuBar();
        JMenu actions = new JMenu("Actions") ;
        JMenuItem displayItem = new JMenuItem(display) ;
        actions.add(displayItem) ;
        JMenuItem playItem = new JMenuItem(play) ;
        actions.add(playItem) ;  
        JMenuItem closeItem = new JMenuItem(close) ;
        actions.add(closeItem) ; 
        menu.add(actions);

        //////////////////////////////////
        ///////// Barre d'options ////////
        //////////////////////////////////
        tools = new JToolBar("Options") ;
        tools.add(display);
        tools.add(play) ;
        tools.add(close) ;

        menu.add(tools) ;

        add(menu, BorderLayout.NORTH);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Affichage et lecture d'objets multimédias");
        pack();
        setVisible(true);

    }

    ///////////////////////////////////////////////////////////////////////
    ////////////// Demande du nom du fichie a chercher/jouer //////////////
    ///////////////////////////////////////////////////////////////////////
    public static String askName() {
        String name = JOptionPane.showInputDialog(null, "Que voulez vous jouer ?", "Media",
                JOptionPane.QUESTION_MESSAGE);
        return name ; 
    }

    public static void affiche(String data) {
        String toBeDisplayed = data + "\nVeuillez faire votre choix :\n" ;
        text_area.append(toBeDisplayed);
    }



//////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////// Classes secondaires pour definir les actions //////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////

class Display extends AbstractAction {

    private static final long serialVersionUID = 4682724243433269236L;

    public Display(final String name) {
        super(name);
    }

    @Override
    public void actionPerformed(final ActionEvent e) {
        String name = RemoteControl.askName();
        if (name == null){
            RemoteControl.affiche("Réessayez");
        }
        else if (name.isEmpty()) {
            RemoteControl.affiche("Vous n'avez rien écrit") ;
        }
        else {
            String response = Client.send("display " + name) ;
            response = response.replaceAll("/nl", "\n") ;
            RemoteControl.affiche(response);
        }
    }

}

class Play extends AbstractAction {

    private static final long serialVersionUID = 5332465269672867890L;

    public Play(final String name) {
        super(name);
    }

    @Override
    public void actionPerformed(final ActionEvent e) {
        String name = RemoteControl.askName() ;
        if (name == null){
            RemoteControl.affiche("Réessayez");
        }
        else if (name.isEmpty()) {
            RemoteControl.affiche("Vous n'avez rien écrit") ;
        }
        else {
            String response = Client.send("play " + name) ;
            RemoteControl.affiche(response);
        }

    }

}

class Close extends AbstractAction {

    private static final long serialVersionUID = 4711581529110940322L;

    public Close(final String name) {
        super(name);
    }

    @Override
    public void actionPerformed(final ActionEvent e) {
            System.exit(0);
    }

}

}


