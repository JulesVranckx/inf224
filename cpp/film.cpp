#include "film.h"

Film::Film(string name, string pathname, int duree, int nbrChapters,  int *  chapters) :
    Video(name, pathname, duree) {
    setChapters(chapters, nbrChapters);
}

const int * Film::getChapters() const {
    return chapters ;
}

int Film::getNbrChapters() const {
    return nbrChapters ;
}

void Film::setChapters(int * newChapters, int newNbrChapters) {


    if (newNbrChapters != nbrChapters) {
        delete []    chapters ;
        chapters = new int[newNbrChapters] ;
    }

    nbrChapters = newNbrChapters ;

    for (int i = 0; i < nbrChapters; i++) {
        chapters[i] = newChapters[i] ;
    }
}

void Film::showVariables(ostream & s) const {

    this->Video::showVariables(s); 
    s << "/nlNombre de chapitres : " << nbrChapters << "/nl" ;

    for (int i = 0; i < nbrChapters; i++){
        s << "Chapitre " << to_string(i) << ": " << to_string(chapters[i]) << "/nl" ;
    }
}


void Film::save(ofstream & f) const{
    string ret = "film\n" + getName() + "\n" + getPathName() + "\n" + to_string(getDuree()) + "\n" + to_string(getNbrChapters()) + "\n" ;
    for (int i = 0; i < getNbrChapters(); i++) {
        ret = ret + to_string(chapters[i]) + "\n" ;
    }
    f << ret ;
}

void Film::load(ifstream & f){ 

    string _name, _pathName, strDuree, strNbrChapters;
    int _duree, _nbrChapters ;
    getline(f, _name);
    getline(f, _pathName); 
    getline(f, strDuree) ;
    getline(f,strNbrChapters);
    _duree = stoi(strDuree);
    _nbrChapters = stoi(strNbrChapters) ;   
    int _chapters[nbrChapters];
    string strCrrtChapter ;
    int crrtChapter ;
    for (int i = 0; i < nbrChapters; i++){
        getline(f, strCrrtChapter) ;
        crrtChapter = stoi(strCrrtChapter);
        _chapters[i] = crrtChapter ;
    }
    name = _name ;
    pathName = _pathName ;
    duree = _duree ; 
    nbrChapters = _nbrChapters ;
    chapters = _chapters ;        
}
