//
// main.cpp
// Created on 21/10/2018
//
#include "multimedia.h"
#include "video.h"
#include "photo.h"
#include "film.h"
#include "group.h"
#include "table.h"
#include <iostream>
#include <memory>
#include <fstream>

using namespace std;

using MulPtr = shared_ptr<Multimedia> ;

int main(int argc, const char* argv[])
{
    /*
     * Test de l'héritage

    Multimedia * l[2] ;
    l[0] = new Video("video_test", "./test.mp4", 10);
    l[1] = new Photo("photo", "./test.jpg", 2, 3);

    for (auto & it : l)  it->play();


    int tab[5] = {1,5,9,3,7};
    Video * v = new Video("video", "video", 25);
    Film * f = new Film("film", "test.mp4", 25, 5, tab) ;
    f->showChapters(cout) ;
    v->showVariables(cout);
    delete f ;
    */

    /*int tab[5] = {1,5,9,3,7};
    MulPtr v (new Video("video", "video", 25));
    MulPtr f (new Film("Has to be delete", "test.mp4", 25, 5, tab)) ;
    Group *  g1 = new Group("Groupe 1") ;
    Group * g2 = new Group("Groupe 2") ;
    g1->push_front(v);
    g1->push_front(f);
    g2->push_front(f);

    g1->display(cout);
    g2->display(cout);

    g1->pop_front();
    g2->pop_front();

    g1->display(cout);

    //Test de la table

    Table * t = new Table() ;

    int tab[5] = {1,5,9,3,7};
    MulPtr f = t->createFilm("film", "./film", 2, 4 , tab) ;
    MulPtr  v = t->createVideo("video", "video", 3) ;
    GrpPtr  g = t->createGroup("group") ;

    g->push_front(f) ;

    t->display("film") ;
    t->display("film");
    t->display("None");
    t->display("group");

    g->display(cout);

    g->pop_front();

    t->displayGrp("film") ; */

    // Debut de la partie client/serveur

    // cree le TCPServer
    shared_ptr<TCPServer> server(new TCPServer());

    // cree l'objet qui gère les données
    shared_ptr<Table> table(new Table());

    int tab[5] = {1,5,9,3,7};
    
    MulPtr  v = table->createVideo("H", "Oups", 3) ;
    MulPtr p = table->createPhoto("f", "test.jpg",5,5) ;
    MulPtr f = table->createFilm("efz", "test.mp4", 2, 5 , tab) ;

    //Serialisation
    table->save() ; 

    /*shared_ptr<Table> tableSerialized(new Table());
    tableSerialized->load("table.txt") ;
    tableSerialized->display("film", cout); */ 

    // le serveur appelera cette méthode chaque fois qu'il y a une requête
    server->setCallback(*table, &Table::processRequest);

    // lance la boucle infinie du serveur
    cout << "Starting Server on port " << PORT << endl;
    int status = server->run(PORT);

    // en cas d'erreur
    if (status < 0) {
      cerr << "Could not start Server on port " << PORT << endl;
      return 1;
    }

    return 0;

}
