#ifndef VIDEO_H
#define VIDEO_H

/*!
 * \file video.h
 * \brief Objet contenant une video dans notre DB
 * \author jules vranckx
 */

#include "multimedia.h"
#include <fstream>

class Table ;
class Film ;  

/*! \class Video
   * \brief classe représentant une video
   *
   *  Permet entre autres de jouer et afficher le fichier
   */


class Video : public Multimedia
{
    friend class Table ;
    friend class Film ;

private:
    //Constructeurs
    Video()  {}
    Video(string name, string pathName, int duree);
   
    int duree = 0; /*!<Duree*/

public:
    
    /*!
     *  \brief Destructeur
     *
     *  Destructeur de la classe Video
     */
    ~Video() ;

    /*!
     *  \brief Get pour la duree
     *
     *  Methode qui permet de retourner la duree de la photo
     * \return int duree
     */
    int getDuree() const ;

    /*!
     *  \brief Set pour Duree
     *
     *  Methode qui permet de set Duree
     *
     *  \param newDuree : le nouveau Duree
     */
    void setDuree(int newDuree);

    /*!
     *  \brief Méthode appelée pour jouer l'objet Multimedia contenu dans l'objet
     */
    void play() const override;

    /*!
     *  \brief Méthode permettant d'afficher les variables
     */
    virtual void showVariables(ostream & s) const ;

    /*!
     *  \brief Méthode pour la serialization - Enregistrement
     */
    void save(ofstream &f) const override ;

    /*!
     *  \brief Méthode pour la serialization - Lecture
     */
    void load(ifstream & f) override ; 
};

#endif // VIDEO_H
