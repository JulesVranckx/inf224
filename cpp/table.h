#ifndef TABLE_H
#define TABLE_H

/*!
 * \file table.h
 * \brief Objet contenant une map pour la gestion cohérente de la DB
 * \author jules vranckx
 */


#include "video.h"
#include "film.h"
#include "photo.h"
#include "multimedia.h"
#include "group.h"

#include <list>
#include <memory>
#include <map>
#include <iostream>
#include <memory>
#include <string>
#include <sstream>
#include "tcpserver.h"
using namespace std;
using namespace cppu;

const int PORT = 3331;


using PhotoPtr = shared_ptr<Photo> ;
using VideoPtr = shared_ptr<Video> ;
using FilmPtr = shared_ptr<Film> ;
using GrpPtr = shared_ptr<Group> ;

/*! \class Table
   * \brief classe contenant la base de données du serveur
   *
   *  Contient des pointeurs vers tous les objets du serveur. 
   */

class Table
{
private :
    map<string, MulPtr> ObjTable ; /*!< Base de données des objets*/
    map<string, GrpPtr> GroupTable ; /*!< Base de données des groupes*/
public:
    //Constructeurs
    Table();

    /*!
     *  \brief Destructeur
     *
     *  Destructeur de la classe Table
     */
    ~Table () {}

    /*!
     *  \brief Crée un smart pointeur contenant une photo
     *
     *  La méthode crée un objet photo, le place dans un smart pointer et l'ajout à la map ObjTable. 
     * 
     * \param name : Le nom
     * \param pathName : le chemin de l'objet
     * \param latitude : dimension verticale de la photo
     * \param longitude : dimension horizontale  de la photo
     * 
     * \return Le smart pointer (pour pouvoir le réutiliser)
     */
    MulPtr createPhoto(string name, string pathName, int latitude, int longitude) ;

    /*!
     *  \brief Crée un smart pointeur contenant une video
     *
     *  La méthode crée un objet video, le place dans un smart pointer et l'ajout à la map ObjTable. 
     * 
     * \param name : Le nom
     * \param pathName : le chemin de l'objet
     * \param duree : la duree de la video
     * 
     * \return Le smart pointer (pour pouvoir le réutiliser)
     */
    MulPtr createVideo(string name, string pathName, int duree) ;

    /*!
     *  \brief Crée un smart pointeur contenant un film
     *
     *  La méthode crée un objet film, le place dans un smart pointer et l'ajout à la map ObjTable. 
     *  Un film est un vidéo divisée en chapitres
     * 
     * \param name : Le nom
     * \param pathName : le chemin de l'objet
     * \param duree : la duree du film
     * \param nbrChapets : le nombre de chapitres du film
     * \param chapters : le tableau à nbrChapters entrées, contenant la durée de chaque chapitre
     * 
     * \return Le smart pointer (pour pouvoir le réutiliser)
     */
    MulPtr createFilm(string name, string pathname, int duree, int nbrChapters, int * chapters );

    /*!
     *  \brief Crée un smart pointeur contenant une groupe
     *
     *  La méthode crée un objet groupe, le place dans un smart pointer et l'ajout à la map GrpTable. 
     * 
     * \param name : Le nom du groupe
     * 
     * \return Le smart pointer (pour pouvoir le réutiliser)
     */
    GrpPtr createGroup(string name) ;

    /*!
     *  \brief Permet l'affichage d'un objet de la map
     *
     *  La méthode affiche l'objet ayant le nom "name". 
     * 
     * \param name : Le nom de l'objet dont on veut connaîtres les caractéristiques
     * 
     * \return Un booléen suivant que l'objet ait été trouvé ou non 
     */
    int displayObj(string name, ostream & s) const ;

    /*!
     *  \brief Permet l'affichage d'un groupe de la map
     *
     *  La méthode affiche le groupeayant le nom "name". 
     * 
     * \param name : Le nom de le groupe dont on veut connaîtres les caractéristiques
     * 
     * \return Un booléen suivant que le groupe ait été trouvé ou non 
     */
    int displayGrp(string name, ostream & s) const;

    /*!
     *  \brief Permet l'affichage d'un élément contenu dans la table
     *
     *  La méthode affiche l'élément ayant le nom "name", en appelant displayGrp et displayObj.
     *  Si l'objet n'est pas trouvé, la fonction renvoie un message d'erreur
     * 
     * \param name : Le nom de l'objet dont on veut connaîtres les caractéristiques

     */
    void display(string name, ostream & s) const ;

    /*!
     *  \brief Méthode appelée pour jouer l'objet Multimedia contenu dans l'objet
     *
     */
    void play(string name, ostream & s) const ;

    /*!
     *  \brief Méthode callBack, appellée à chaque nouvelle requête envoyée par le client 
     */
    bool processRequest(TCPConnection& cnx, const string& request, string& response) ;

    /*!
     *  \brief Méthode pour la serialization - Enregistrement
     */
    void save() const ;

    /*!
     *  \brief Méthode pour la serialization - Lecture
     * 
     * \bug La fonction génère une erreur: Un std::invalid argument
     */
    void load(string fileName) ;

};

#endif // TABLE_H
