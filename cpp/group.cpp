#include "group.h"

Group::Group(string name) : list<MulPtr>(), name(name) {}

string Group::getName() const {
    return name ;
}

void Group::display(ostream & s) const {

    s << "Display content of group :" << name << endl ;

    for (auto & it : *this) {
        it->showVariables(s) ;
    s << "End of " << name << "\n";
    }
}
