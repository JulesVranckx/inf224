#include "photo.h"

Photo::Photo(string name, string pathName, int _latitude, int _longitude) :
    Multimedia(name, pathName), latitude(_latitude),  longitude(_longitude) {}

Photo::~Photo() {} 

void Photo::setLatitude(int newLatitude) {
    latitude = newLatitude ;
}

void Photo::setLongitude(int newLongitude) {
    longitude = newLongitude ;
}

int Photo::getLatitude() const {
    return latitude ;
}

int Photo::getLongitude() const {
    return longitude ;
}

void Photo::play() const {
    system(("eog " + this->getPathName() +"&").c_str());
}

void Photo::showVariables(ostream & s) const {
    s << "Photo : " << name << " - Taille :" << to_string(latitude) << "x" << to_string(longitude);
}

void Photo::save(ofstream &f ) const{
    f << "photo\n" + getName() << "\n" << getPathName() << "\n" << to_string(getLatitude()) << "\n" << to_string(getLongitude()) << "\n" ;
}

void Photo::load(ifstream & f) {
    string name, pathName, strLat, strLong ;
    int latitude, longitude ;
    getline(f, name);
    getline(f, pathName); 
    getline(f, strLat);
    getline(f,strLong) ;
    cout << strLat ; 
    latitude = stoi(strLat) ;
    longitude = stoi(strLong);
    name = name ;
    pathName = pathName ;
    latitude = latitude ;
    longitude = longitude ;
    cout << "Here2" << endl ;
}
