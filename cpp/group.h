#ifndef GROUP_H
#define GROUP_H

/*!
 * \file group.h
 * \brief Objet contenant un groupe dans notre DB
 * \author jules vranckx
 */

#include "video.h"
#include "film.h"
#include "photo.h"
#include "multimedia.h"

#include <list>
#include <memory>
#include <iostream> 


using MulPtr = shared_ptr<Multimedia> ;

/*! \class Group
   * \brief classe representant un groupe 
   *
   *  Permet de mettre ensemble des films, videos et photos
   */

class Group : public list<MulPtr>
{
private :
    string name ; /*!< Nom*/
public:
    //Constructeurs
    Group() : list<MulPtr>() {}
    Group(string name) ;

    /*!
     *  \brief Get pour le nom
     *
     *  Methode qui permet de retourner le nom du groupe
     */
    string getName() const ;

    /*!
    * \brief Permet d'afficher les paramètres des objets du groupe
    */
    void display(ostream & s) const;

};

#endif // GROUP_H
