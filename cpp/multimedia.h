#ifndef MULTIMEDIA_H
#define MULTIMEDIA_H

/*!
 * \file multimedia.h
 * \brief Classe abstraite de base pour tous les objets mulitmedia
 * \author jules vranckx
 */

#include <string>
#include <iostream>
using namespace std;

class Table ;
class Video ;
class Photo ;
class Film ;

/*! \class Mulitmedia
   * \brief classe representant un objet multimedia 
   *
   *  Class abstraite
   */

class Multimedia
{
    friend class Table ;
    friend class Film ; 
    friend class Video ; 
    friend class Photo ; 

private :
    string name;  /*!< Nom*/
    string pathName ; /*!< Chemin d'accès en local*/
    // Constructeurs
    Multimedia() {}
    Multimedia(string , string) ;
    
    

public :

    /*!
     *  \brief Destructeur
     *
     *  Destructeur de la classe Multimedia
     */
    virtual ~Multimedia() ;  

    /*!
     *  \brief Get pour le nom
     *
     *  Methode qui permet de retourner le nom du multimedia
     */
    string getName() const ;

    /*!
     *  \brief Get pour le chemin du fichier
     *
     *  Methode qui permet de retourner le chemin du fichier
     */
    string getPathName() const ;

    /*!
     *  \brief Set pour name
     *
     *  Methode qui permet de set name
     *
     *  \param newName : le nouveau nom 
     */
    void setName(string newName) ;

    /*!
     *  \brief Set pour pathName
     *
     *  Methode qui permet de set pathName
     *
     *  \param newPathName : le nouveau pathName
     */
    void setPathName( string newPathName);

    /*!
     *  \brief Méthode permettant d'afficher les variables
     */
    virtual void showVariables(ostream & s) const = 0 ; 

    /*!
     *  \brief Méthode appelée pour jouer l'objet Multimedia contenu dans l'objet
     */
    virtual void play() const {} 
    

    /*!
     *  \brief Méthode pour la serialization - Enregistrement
     */
    virtual void save(ofstream & f) const {} 

    /*!
     *  \brief Méthode pour la serialization - Lecture
     */
    virtual void load (ifstream & f) {}
};

#endif // MULTIMEDIA_H
