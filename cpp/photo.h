#ifndef PHOTO_H
#define PHOTO_H

/*!
 * \file photo.h
 * \brief Objet contenant une photo dans notre DB
 * \author jules vranckx
 */

#include "multimedia.h"
#include <fstream>

class Table ;

/*! \class Photo
   * \brief classe représentant une photo
   *
   *  Permet entre autres de jouer et afficher le fichier
   */


class Photo : public Multimedia
{
    friend class Table ; 
    friend class Multimedia ; 
private:
    int latitude = 0 ;
    int longitude = 0 ;
    //Constructeurs
    Photo() {}
    Photo(string name, string pathName, int latitude, int longitude);

public:
    

    /*!
     *  \brief Set pour latitude
     *
     *  Methode qui permet de set latitude
     *
     *  \param newLatitude : le nouveau latitude
     */
    void setLatitude(int newLatitude);

    /*!
     *  \brief Set pour longitude
     *
     *  Methode qui permet de set longitude
     *
     *  \param newLongitude : le nouveau longitude
     */
    void setLongitude(int newLongitude);

    /*!
     *  \brief Get pour la latitude
     *
     *  Methode qui permet de retourner la latitude de la photo
     * \return int latitude
     */
    int getLatitude() const ;

    /*!
     *  \brief Get pour la longitude
     *
     *  Methode qui permet de retourner la longitude de la photo
     * \return int longitude
     */
    int getLongitude() const ;

    /*!
     *  \brief Destructeur
     *
     *  Destructeur de la classe Photo
     */
    ~Photo() ;  

    /*!
     *  \brief Méthode appelée pour jouer l'objet Multimedia contenu dans l'objet
     */
    void play() const override ;

    /*!
     *  \brief Méthode permettant d'afficher les variables
     */
    void showVariables(ostream & s) const override ;

    /*!
     *  \brief Méthode pour la serialization - Lecture
     */
    void load(ifstream &f) override; 

    /*!
     *  \brief Méthode pour la serialization - Enregistrement
     */
    void save(ofstream &f) const override ;
};

#endif // PHOTO_H
