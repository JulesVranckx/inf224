#include "multimedia.h"

Multimedia::Multimedia(string _name, string _pathName) {
    name = _name ;
    pathName = _pathName ; 
}

Multimedia::~Multimedia() {
    cout << "Destruction de l'objet " << getName() << endl ;
} 

string Multimedia::getName() const {
    return name ;
}

string Multimedia::getPathName() const {
    return pathName ;
}

void Multimedia::setName(string newName) {
    name = newName ;
}

void Multimedia::setPathName(string newPathName) {
    pathName = newPathName ;
}

