#ifndef FILM_H
#define FILM_H

/*!
 * \file film.h
 * \brief Objet contenant un film dans notre DB
 * \author jules vranckx
 */

#include "video.h"
#include <fstream>



class Table; 

/*! \class Film
   * \brief classe representant un film 
   *
   *  Permet entre autre de lire et afficher les attributs du fichier
   */


class Film : public Video
{
    friend class Table; 

private:
    int nbrChapters = 0; /*!< Nombre de chapitres*/
    int * chapters = nullptr ; /*!< Liste des chapitres*/
    //Contructeurs
    Film() {}
    Film(string name, string pathName, int duree, int nbrChapters, int * chapters );

public:
    
    /*!
     *  \brief Destructeur
     *
     *  Destructeur de la classe Film
     */
    ~Film() { delete [] chapters;
              chapters = nullptr ;
            }

    /*!
     *  \brief Set pour chapters
     *
     *  Methode qui permet de set chapters
     *
     *  \param newChapters : le tableau des chapitres 
     *  \param NewNbrChapters : le nombre de chapitres
     */
    void setChapters(int * newChapters, int newNbrChapters);

    /*!
     *  \brief Get pour chapters
     *
     *  Methode qui permet de get chapters
     */
    const int * getChapters() const ;

    /*!
     *  \brief Get pour chapters
     *
     *  Methode qui permet de get le nombre de chapters
     */
    int getNbrChapters() const ;

    /*!
     *  \brief Méthode permettant d'afficher les variables
     */

    void showVariables(ostream & s) const override ;

    /*!
     *  \brief Méthode pour la serialization - Enregistrement
     */
    void save(ofstream &f) const override ;

    /*!
     *  \brief Méthode pour la serialization - Lecture
     */
    void load(ifstream & f) override ; 


};

#endif // FILM_H
