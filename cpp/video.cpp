    #include "video.h"

Video::Video(string _name, string _pathName, int _duree) : Multimedia(_name, _pathName), duree(_duree) {}

Video::~Video() {} 

int Video::getDuree() const {
    return duree;
}

void Video::setDuree(int newDuree) {
    duree = newDuree ;
}

void Video::play() const{
    system(("mpv " + this->getPathName() + "&").c_str());
}


void Video::save(ofstream &f) const {
    f <<"video\n" ;
    f << getName() << endl ;
    f << getPathName() << endl ;
    f << to_string(getDuree()) << endl ;
}

void Video::showVariables(ostream & s) const {
    s << "Video : " << name << " - Duree :" << to_string(duree);
}


void Video::load(ifstream & f) { 
    string name, pathName, strDuree;
    int duree ;
    getline(f, name);
    getline(f, pathName); 
    getline(f, strDuree) ;
    cout << strDuree ;
    duree = stoi("45") ; 
    name = name ;
    pathName = pathName ;
    duree = duree ; 
    cout << "Here" << endl  ; 
}