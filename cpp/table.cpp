#include "table.h"

Table::Table(){}

GrpPtr Table::createGroup(string name){
        GrpPtr g(new Group(name)) ;
        GroupTable[name] =  g ;
        return g ;
}

MulPtr Table::createPhoto(string name, string pathName, int latitude, int longitude){
    PhotoPtr p(new Photo(name, pathName, latitude, longitude)) ;
    ObjTable[name] = p ;
    return p ;
}

MulPtr Table::createVideo(string name, string pathName, int duree){
    VideoPtr v(new Video(name, pathName,duree)) ;
    ObjTable[name] = v ;
    return v ;
}

MulPtr Table::createFilm(string name, string pathname, int duree, int nbrChapters, int *chapters){
    FilmPtr f(new Film(name, pathname, duree, nbrChapters, chapters)) ;
    ObjTable[name] = f ;
    return f ;
}

int Table::displayObj(string name, ostream & s) const {
    if (ObjTable.find(name) != ObjTable.end()){
        ObjTable.at(name)->showVariables(s);
        return 1 ;
    }
    else
        return 0 ;
}

int Table::displayGrp(string name, ostream & s) const{
    if (GroupTable.find(name) != GroupTable.end()){
        GroupTable.at(name)->display(s);
        return 1 ;
    }
    else
        return 0 ;
}

void Table::display(string name, ostream & s) const {
    if (displayGrp(name,s) || displayObj(name,s)){return ;}
    else s << "Erreur, fichier introuvable" ;
}

void Table::play(string name, ostream & s) const {
    if (ObjTable.find(name) != ObjTable.end()) {
        ObjTable.at(name)->play();
    }
    else s << " -- Oups non, fichier introuvable" ;
}

/* Cette méthode est appelée chaque fois qu'il y a une requête à traiter.
 * Ca doit etre une methode de la classe qui gere les données, afin qu'elle
 * puisse y accéder.
 *
 * Arguments:
 * - 'request' contient la requête
 * - 'response' sert à indiquer la réponse qui sera renvoyée au client
 * - si la fonction renvoie false la connexion est close.
 *
 * Cette fonction peut etre appelée en parallele par plusieurs threads (il y a
 * un thread par client).
 *
 * Pour eviter les problemes de concurrence on peut créer un verrou en creant
 * une variable Lock **dans la pile** qui doit etre en mode WRITE (2e argument = true)
 * si la fonction modifie les donnees.
 */
bool Table::processRequest(TCPConnection& cnx, const string& request, string& response)
{
  cerr << "\nRequest: '" << request << "'" << endl;

  // 1) pour decouper la requête:
  // on peut par exemple utiliser stringstream et getline()

  string name, action ;

  stringstream requestStream(request) ;
  getline(requestStream, action, ' ') ;
  getline(requestStream, name, ' ') ;


  // 2) faire le traitement:
  // - si le traitement modifie les donnees inclure: TCPLock lock(cnx, true);
  // - sinon juste: TCPLock lock(cnx); Pas besoin pour le moment, on joue ou affiche seulement

  stringstream  responseStream ;

  if (action.empty() ||  name.empty()){
      response = "Please use 'play file' or 'display file'" ;
  }

  if (action.compare("play") == 0){
      this->play(name, responseStream);
      response = responseStream.str() ;
  }
  else if (action.compare("display") == 0) {
      this->display(name, responseStream);
      response = responseStream.str() ;
  }
  else {
      response = "Commande introuvable - Please use 'play' or 'display'";
  }

  // 3) retourner la reponse au client:
  // - pour l'instant ca retourne juste OK suivi de la requête
  // - pour retourner quelque chose de plus utile on peut appeler la methode print()
  //   des objets ou des groupes en lui passant en argument un stringstream
  // - attention, la requête NE DOIT PAS contenir les caractères \n ou \r car
  //   ils servent à délimiter les messages entre le serveur et le client

  cerr << "response: " << response << endl;

  // renvoyer false si on veut clore la connexion avec le client
  return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
////////                           Serialisation                                            ////////
////////////////////////////////////////////////////////////////////////////////////////////////////
void Table::save() const {
    ofstream file ("table.txt") ;
    for (auto it : ObjTable){
        it.second->save(file) ;
    }
    file.close() ;
}

void Table::load(string fileName) {
    ifstream f(fileName) ; 

    while(f) {
        string classType ; 
        getline(f, classType) ; 

        if (classType.compare("") != 0){

            if (classType.compare("photo")) {
                PhotoPtr photo (new Photo())  ; 
                photo->load(f) ;
                ObjTable[photo->getName()] = photo ; 
            }
            else if (classType.compare("video")){
                VideoPtr video (new Video())  ; 
                video->load(f) ;
                ObjTable[video->getName()] = video ; 
                
            }
            else if (classType.compare("film")){ 
                FilmPtr film (new Film())  ; 
                film->load(f) ;
                ObjTable[film->getName()] = film ; 
                
            }
            else {
                cerr << "Erreur : classe inconnue" << endl ; 
                break ; 
            }
        }
    }
}







